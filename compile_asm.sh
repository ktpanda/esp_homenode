#!/bin/bash
for fn; do
    base=${fn%.*}

    ./debug_compile.sh \
        -c -w -Os -g -mlongcalls -mtext-section-literals -fno-rtti \
        -falign-functions=4 -std=gnu++11 -ffunction-sections \
        -fdata-sections -fno-exceptions  \
        "$fn" \
        -S -o - > $base.rawasm.txt

    $HOME/mhome/bin/addlines < $base.rawasm.txt > $base.asm.txt
    rm $base.rawasm.txt
done
