#!/bin/sh
arduino=$HOME/.arduino15
packages=$arduino/packages
esp=$packages/esp8266
esphw=$esp/hardware/esp8266/2.7.4

exec "$esp/tools/xtensa-lx106-elf-gcc/2.5.0-4-b40a506/bin/xtensa-lx106-elf-g++" \
    -D__ets__ -DICACHE_FLASH -U__STRICT_ANSI__ \
    -DNONOSDK22x_190703=1 -DF_CPU=80000000L -DLWIP_OPEN_SRC -DTCP_MSS=536 \
    -DLWIP_FEATURES=1 -DLWIP_IPV6=0   -DARDUINO=10802 -DARDUINO_ESP8266_NODEMCU -DARDUINO_ARCH_ESP8266 \
    -DARDUINO_BOARD="ESP8266_NODEMCU" \
    -DLED_BUILTIN=2 -DFLASHMODE_DIO -DESP8266 \
    "-I$esphw/tools/sdk/include" \
    "-I$esphw/tools/sdk/lwip2/include" \
    "-I$esphw/tools/sdk/libc/xtensa-lx106-elf/include" \
    "-I$esphw/libraries/EEPROM" \
    "-I$esphw/cores/esp8266" \
    "-I$esphw/variants/nodemcu" \
    "-I$esphw/libraries/ESP8266WiFi/src" \
    "-I$HOME/Arduino/libraries/OneWire" \
    "$@"
