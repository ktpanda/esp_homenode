#!/bin/sh
IFS=

cd `dirname $0`

force=
if [ "$1" = -f ]; then
    force=1
    shift
fi

check=
if [ "$1" = -c ]; then
    check=1
    shift
fi

port=$1
[ -z "$port" ] && port=/dev/ttyUSB0

if [ ! -c "$port" ]; then
    echo "Port $port does not exist."
    exit 1
fi

make || exit $?

latest_version=`cat build/build_date.txt`
if [ -z "$latest_version" ]; then
    echo "Could not determine latest build version"
    exit 1
fi

current_flashed_version=`./read_version.sh $port`
echo "Current build date    : $latest_version"
if [ -n "$current_flashed_version" ]; then
    echo "Controller build date : $current_flashed_version"
else
    echo "Controller build date : <could not read>"
fi

if [ -z "$force" ]; then
    if [  -z "$current_flashed_version" ]; then
        echo "Could not read version from $port."
        echo "Use -f to try to upload anyway"
        exit 0
    fi

    if [ "$latest_version" = "$current_flashed_version" ]; then
        echo "No update needed."
        echo "Use -f to upload anyway"
        exit 0
    fi
fi
if [ -n "$check" ]; then
    echo "$port needs update"
else
    exec arduino-cli upload --input-dir build --verbose -b esp8266:esp8266:nodemcuv2 esp_homenode --port $port
fi
