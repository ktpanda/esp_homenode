#!/bin/bash
set -e
port=$1
if [ -z "$port" ]; then
    echo "usage: $0 port" >&2
    exit 1
fi

if ! [ -c "$port" ]; then
    echo "Port $port does not exist or is not a device" >&2
    exit 1
fi

exec 3<>$port
stty 115200 min 1 time 0 -parenb -parodd -cmspar cs8 hupcl -cstopb cread clocal \
     -crtscts -ignbrk -brkint -ignpar -parmrk -inpck \
     -istrip -inlcr -igncr -icrnl -ixon -ixoff -iuclc -ixany -imaxbel -iutf8 \
     -opost -olcuc -ocrnl onlcr -onocr -onlret -ofill -ofdel \
     -isig -icanon -iexten -echo echoe echok -echonl <&3

echo -ne '\003version\r' >&3
while read -u 3 -n 100 -t 1 x; do
    if [[ $x == *BUILD_DATE=* ]]; then
        x=${x/$'\015'/}
        echo "${x##*=}"
        break
    fi
done
