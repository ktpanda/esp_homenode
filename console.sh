#!/bin/sh
cd `dirname $0`
tty=/dev/ttyUSB0
while [ ! -r $tty ]; do
    sleep .05
done
exec picocom -e v $tty --imap lfcrlf --baud 115200
