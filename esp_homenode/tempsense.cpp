#include "esp_homenode.h"
#include "tempsense.h"

#if ENABLE_TEMPSENSE

#include <OneWire.h>

// Value indicating no temperature can be read
enum {
    TEMP_DISABLED = -0x8000,
    TEMP_NOT_PRESENT = -0x7FFF,
    TEMP_READ_ERROR = -0x7FFE,
    TEMP_CONVERT_ERROR = -0x7FFD
};

bool tempsense_active = false;

// micros() of the next temperature sensor reading
uint32_t nexttemp;

OneWire ds(TEMPSENSE_PIN);

class DS18B20 {
public:
    byte addr[8];
    int16_t temp;

    void set_addr(uint32_t h, uint32_t l) {
        addr[0] = (h >> 24) & 0xFF;
        addr[1] = (h >> 16) & 0xFF;
        addr[2] = (h >> 8) & 0xFF;
        addr[3] = (h) & 0xFF;
        addr[4] = (l >> 24) & 0xFF;
        addr[5] = (l >> 16) & 0xFF;
        addr[6] = (l >> 8) & 0xFF;
        addr[7] = (l) & 0xFF;
    }

    void read_temp() {
        uint8_t data[9];
        char present = ds.reset();
        if (!present) {
            temp = TEMP_NOT_PRESENT;
            return;
        }
        ds.skip();
        ds.write(0xBE);
        for (int i = 0; i < 9; i++) {
            data[i] = ds.read();
        }
        byte crc = OneWire::crc8(data, 8);
        if (data[8] != crc || data[4] == 0) {
            temp = TEMP_READ_ERROR;
            return;
        }

        temp = (data[1] << 8) | data[0];
        // If conversion failed...
        if (temp == 0x0550) temp = TEMP_CONVERT_ERROR;
    }

};

DS18B20 tempsense;

void tempsense_init() {
    tempsense.temp = TEMP_DISABLED;
}

void send_convert_t() {
    ds.reset();
    ds.skip();
    ds.write(0x44, 1);
}

inline uint32_t tempsense_interval() {
    uint16_t intv_ms = config_data.tempsense_interval;
    if (intv_ms == 0) {
        intv_ms = 800;
    }
    if (intv_ms < 100) intv_ms = 100;
    if (intv_ms > 5000) intv_ms = 5000;
    return 1000 * intv_ms;
}

void tempsense_check() {
    bool want_active = wifi_connected && config_data.tempsense_port > 0 && config_data.tempsense_dest > 0;
    if (want_active != tempsense_active) {
        if (want_active) {
            async_message(Serial, "Thermometer active");
            nexttemp = micros() + tempsense_interval();
            send_convert_t();
        } else {
            async_message(Serial, "Thermometer inactive");
            tempsense.temp = TEMP_DISABLED;
        }
        tempsense_active = want_active;
    }

    if (!tempsense_active)
        return;

    uint32_t ctime = micros();
    if (time_since(nexttemp, ctime) >= 0) {
        IPAddress temp_ip(config_data.tempsense_dest);
        nexttemp = ctime + tempsense_interval();

        tempsense.read_temp();
        udp.beginPacket(temp_ip, config_data.tempsense_port);
        write16(tempsense.temp);
        udp.endPacket();
        send_convert_t();
    }
}

bool command_temp(CommandCtx& ctx, char* param UNUSED, int param_len UNUSED) {
    switch (tempsense.temp) {
        case TEMP_DISABLED: ctx.out.println("Sensor disabled"); break;
        case TEMP_NOT_PRESENT: ctx.out.println("Sensor not connected"); break;
        case TEMP_READ_ERROR: ctx.out.println("Sensor read error"); break;
        case TEMP_CONVERT_ERROR: ctx.out.println("Sensor failed to convert temperature"); break;
        default: {
            float temp_c = tempsense.temp / 16.0;
            float temp_f = temp_c * 1.8 + 32;
            ctx.out.printf("Temperature: %04X, %.2fC, %.2fF\r\n", tempsense.temp, temp_c, temp_f);
        }
    }
    return true;
}

#else

void tempsense_check() { }

#endif
