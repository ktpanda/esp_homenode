/* -*- mode: c++ -*-
 */

#ifndef ESP_HOMENODE_H
#define ESP_HOMENODE_H

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

// Set these to 0 to permanently disable the functionality
#ifndef ENABLE_RMTRECV
#define ENABLE_RMTRECV 1
#endif

#ifndef ENABLE_TEMPSENSE
#define ENABLE_TEMPSENSE 1
#endif

#define UNUSED __attribute__((unused))

////////////////////////////////////////////////////////////////////////////////
// Pin definitions
////////////////////////////////////////////////////////////////////////////////

static const uint8_t NUM_GPIOS = 11;
static const uint8_t LED1 = D0; // Bright blue LED
static const uint8_t LED2 = D4; // Secondary LED

static const uint8_t GPIO_PINS[NUM_GPIOS] = {D0, D1, D2, D3, D4, D5, D6, D7, D8, D9, D10};

// 433 Mhz RF receiver
static const uint8_t RFPIN = D1;

// DS1820 temperature sensor
static const uint8_t TEMPSENSE_PIN = D5;

inline bool valid_gpio(int gpio) {
    return (0 < gpio && gpio <= NUM_GPIOS);
}

inline uint8_t gpio_pin(int gpio) {
    return GPIO_PINS[gpio - 1];
}

////////////////////////////////////////////////////////////////////////////////
// Wifi
////////////////////////////////////////////////////////////////////////////////

extern WiFiUDP udp;

extern bool wifi_enabled;

extern bool wifi_connected;

template<typename T>
inline void write8(T v) {
    udp.write(0xFF & (v));
}

template<typename T>
inline void write16(T v) {
    udp.write(0xFF & (v >> 8));
    udp.write(0xFF & (v));
}

template<typename T>
inline void write32(T v) {
    udp.write(0xFF & (v >> 24));
    udp.write(0xFF & (v >> 16));
    udp.write(0xFF & (v >> 8));
    udp.write(0xFF & (v));
}

inline uint16_t read16() {
    uint8_t hi = udp.read();
    uint8_t lo = udp.read();
    return (hi << 8) | lo;
}


////////////////////////////////////////////////////////////////////////////////
// Config
////////////////////////////////////////////////////////////////////////////////

static const int NUM_OUTPUTS = 4;

static const uint16_t CONFIG_SIG = 0x1546;
static const int WIFI_SSID_MAX = 32;
static const int WIFI_PASSWD_MAX = 64;

extern bool serial_debug;

struct RelayConfig {
    uint8_t relay_pin;
    uint8_t led_pin;
    uint8_t button_pin;
    uint8_t drive:1;
    uint8_t led_invert:1;
    uint8_t reserved:6;
};

struct ConfigData {
    uint16_t size;
    char wifi_ssid[WIFI_SSID_MAX + 1];
    char wifi_passwd[WIFI_PASSWD_MAX + 1];
    uint32_t tempsense_dest, remote_dest;
    uint16_t tempsense_port, remote_port;
    uint16_t tempsense_interval;
    uint8_t debounce_centisecs;
    uint32_t gpio_dest;
    uint16_t gpio_port;
    RelayConfig relays[NUM_OUTPUTS];
    uint8_t led_onlevel, led_offlevel;
};


struct ConfigData_1545 {
    uint16_t size;
    char wifi_ssid[WIFI_SSID_MAX + 1];
    char wifi_passwd[WIFI_PASSWD_MAX + 1];
    uint32_t tempsense_dest, remote_dest;
    uint16_t tempsense_port, remote_port;
    uint16_t tempsense_interval;
    uint8_t gpio_drive;
    uint8_t debounce_centisecs;
    uint32_t gpio_dest;
    uint16_t gpio_port;
    uint8_t button_gpio[2];
    uint8_t reserved[2];
};

extern ConfigData config_data;

////////////////////////////////////////////////////////////////////////////////
// Commands
////////////////////////////////////////////////////////////////////////////////

struct CommandCtx {
    CommandCtx(Print& out, bool is_net=false) : out(out), is_net(is_net) { }
    Print& out;
    bool is_net;
};

extern bool handle_command_line(CommandCtx& ctx, char* cmdline, int cmdline_len);
extern bool handle_command(CommandCtx& ctx, char* cmd, int len, char* param, int param_len);

extern void clear_input_buffer();

////////////////////////////////////////////////////////////////////////////////
// Message printing
////////////////////////////////////////////////////////////////////////////////

extern void clear_prompt();
extern bool debug_begin(int type);

#define print_const(out, text) _print_const(out, text, sizeof(text) - 1)
extern void _print_const(Print& out, const char* text, int size);

template<typename T1>
inline void async_message(Print& out, T1 v1) {
    clear_prompt();
    out.println(v1);
}

template<typename T1, typename T2>
inline void async_message(Print& out, T1 v1, T2 v2) {
    clear_prompt();
    out.print(v1);
    out.println(v2);
}

template<typename T1, typename T2, typename T3>
inline void async_message(Print& out, T1 v1, T2 v2, T3 v3) {
    clear_prompt();
    out.print(v1);
    out.print(v2);
    out.println(v3);
}

template<typename T>
inline void print_quoted(Print& out, const char* msg, T val) {
    out.print(msg);
    out.print(" \"");
    out.print(val);
    out.println('"');
}

template<typename T>
inline void print_unquoted(Print& out, const char* msg, T val) {
    out.print(msg);
    out.print(' ');
    out.println(val);
}

////////////////////////////////////////////////////////////////////////////////
// Inline utility functions
////////////////////////////////////////////////////////////////////////////////

inline uint16_t saturate(uint32_t value) {
    return value > 0xFFFF ? 0xFFFF : (uint16_t)value;
}

inline uint8_t queue_incr(uint8_t pos, int amt=1) {
    return (pos + (amt & 63)) & 63;
}

inline bool check_bound(int32_t val, int32_t min, int32_t max) {
    return min <= val && val <= max;
}

inline int32_t time_since(uint32_t start, uint32_t ctime) {
    return ((int32_t)ctime - (int32_t)start);
}

inline int32_t time_since(uint32_t start) {
    return time_since(start, micros());
}


#endif
