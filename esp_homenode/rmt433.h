#ifndef ESP_RMT433_H
#define ESP_RMT433_H

extern void rmt433_init();
extern void rmt433_check();
extern bool rmt433_can_cpu_idle();
extern void rmt433_set_debug(const IPAddress& raddr, uint16_t rport, int arg1, int arg2);

#endif
