#include "esp_homenode.h"
#include "rmt433.h"

#if ENABLE_RMTRECV

#ifndef ICACHE_RAM_ATTR
#define ICACHE_RAM_ATTR
#endif

// Minimum length of a mark or space. Anything shorter than this is considered a "bounce"
static const int MIN_MARK_LENGTH = 100;
static const int MIN_SPACE_LENGTH = 130;

// Minimum and Maximum length for initial pulse (6 - 9 ms)
static const uint32_t START_MARK_MIN = 160;
static const uint32_t START_MARK_MAX = 1000;
static const uint32_t START_SPACE_MIN = 6000;
static const uint32_t START_SPACE_MAX = 10000;

// If the interrupt is active
bool rmtrecv_active = false;

// The last state of the input pin
bool last_pin_state = true;

// Tracks timing of each full wave received
struct Bit {
    uint16_t mark; // Duration that the pin was high (microseconds)
    uint16_t space; // Duration that the pin was low

    inline uint32_t duration() const { return mark + space; }
};

// micros() when the input last changed
uint32_t last_change_time;

// Duration in microseconds of the last pulse
int32_t last_mark_duration, last_space_duration;

// Raw pulse timings, before debouncing
static const unsigned int MAX_RAW_TIMINGS = 32;
Bit raw_pulse_timings[MAX_RAW_TIMINGS];
uint16_t num_raw_timings = 0;

// Number of interrupts where the value of the pin didn't change
uint16_t spurious_interrupts = 0;

// Circular timing queue, debounced
Bit timing_queue[64];
volatile uint8_t timing_queue_head = 0;
uint8_t timing_queue_tail = 0;

// Last successfully decoded RF code
uint32_t current_code = 0;

// Last code received with no bad bits
uint32_t last_good_code = 0;

// Time the last good code was received
uint32_t last_good_code_time = 0;


////////////////////////////////////////////////////////////////////////////////
// RF debug
////////////////////////////////////////////////////////////////////////////////

// Debug destination. Activated when someone sends a debug enable packet.
IPAddress debug_dest_addr(0, 0, 0, 0);
uint16_t debug_dest_port = 0;


// Number of packets remaining.
uint8_t debug_npackets = 0;

// Flags sent by the debug client indicating which data is requested.
uint8_t debug_flags = 0;

enum DebugType {
    DEBUG_RAW_TIMINGS = 0,
    DEBUG_DECODED_TIMINGS = 1,
    DEBUG_CODES = 2,
};

// Time when we sent the last debug packet
uint32_t last_debug_packet = 0;

bool debug_begin(DebugType type) {
    if (debug_flags & (1 << type)) {
        if (!--debug_npackets) {
            debug_flags = 0;
        }
        udp.beginPacket(debug_dest_addr, debug_dest_port);
        write8(0xF0 | type);
        return true;
    }
    return false;
}

void rmt433_set_debug(const IPAddress& raddr, uint16_t rport, int arg1, int arg2) {
    debug_dest_addr = raddr;
    debug_dest_port = rport;
    debug_npackets = arg1;
    debug_flags = arg2;
    last_debug_packet = micros();
}

////////////////////////////////////////////////////////////////////////////////
// RF functions
////////////////////////////////////////////////////////////////////////////////

void rmt433_init() {

}

void ICACHE_RAM_ATTR rf_interrupt() {
    bool pin = digitalRead(RFPIN) != LOW;
    unsigned long ctime = micros();
    if (pin == last_pin_state) {
        spurious_interrupts++;
        return;
    }
    last_pin_state = pin;

    // Low to High
    if (pin) {
        last_space_duration = time_since(last_change_time, ctime);

        if (debug_flags & (1<<DEBUG_RAW_TIMINGS) && num_raw_timings < MAX_RAW_TIMINGS) {
            Bit& b(raw_pulse_timings[num_raw_timings]);
            b.mark = saturate(last_mark_duration);
            b.space = saturate(last_space_duration);
            num_raw_timings++;
        }

        Bit& prev(timing_queue[timing_queue_head]);
        if (prev.space < MIN_SPACE_LENGTH) {
            prev.mark = saturate(prev.mark + prev.space + last_mark_duration);
            prev.space = saturate(last_space_duration);
        } else if (last_mark_duration < MIN_MARK_LENGTH) {
            prev.space = saturate(prev.space + last_mark_duration + last_space_duration);
        } else {
            timing_queue_head = queue_incr(timing_queue_head);
            Bit& cur(timing_queue[timing_queue_head]);
            cur.mark = saturate(last_mark_duration);
            cur.space = saturate(last_space_duration);
        }

    } else {
        last_mark_duration = time_since(last_change_time, ctime);
    }

    last_change_time = ctime;
}

void reset_rf() {
    last_change_time = micros();
    last_mark_duration = 1000;
    last_space_duration = 1000;

    // Fill the queue with some sane defaults
    timing_queue_head = 0;
    for (int i = 0; i < 64; i++) {
        timing_queue[i].mark = 1000;
        timing_queue[i].space = 1000;
    }
}

void check_rmtrecv_active() {
    bool want_active = wifi_connected && config_data.remote_port > 0 && config_data.remote_dest > 0;
    if (want_active == rmtrecv_active)
        return;

    if (want_active) {
        async_message(Serial, "433 Mhz active");
        pinMode(RFPIN, INPUT);
        reset_rf();
        last_pin_state = digitalRead(RFPIN) != LOW;
        attachInterrupt(digitalPinToInterrupt(RFPIN), rf_interrupt, CHANGE);
        debug_flags = 0;
    } else {
        async_message(Serial, "433 Mhz inactive");
        detachInterrupt(digitalPinToInterrupt(RFPIN));
        debug_flags = 0;
    }
    rmtrecv_active = want_active;
}

bool rmt433_can_cpu_idle() { return !rmtrecv_active; }

inline bool check_bit_bound(uint32_t duration, int32_t lower_bound, int32_t upper_bound, int32_t length_error) {
    return length_error > 0 ?
        check_bound(duration, lower_bound - length_error, upper_bound) :
        check_bound(duration, lower_bound, upper_bound - length_error);
}

static void check_timing_queue() {
    // More than 10 seconds without an interrupt? Maybe receiver isn't connected
    if (wifi_connected && time_since(last_change_time) > 10000000) {
        noInterrupts();
        reset_rf();
        interrupts();
    }

    if (last_good_code && time_since(last_good_code_time) > 300000) {
        last_good_code = 0;
    }

    if (timing_queue_head == timing_queue_tail) {
        return;
    }

    timing_queue_tail = queue_incr(timing_queue_tail);

    // Look back 32 positions
    uint8_t startpos = queue_incr(timing_queue_tail, -32);

    Bit start = timing_queue[startpos];
    if (!check_bound(start.space, START_SPACE_MIN, START_SPACE_MAX) && !check_bound(start.mark, START_MARK_MIN, START_MARK_MAX)) {
        return;
    }

    uint32_t code = 0;
    uint32_t badbits = 0;
    uint32_t code_dur = 0;

    int nbadbits = 0;

    for (int i = 0, curpos = startpos; i < 24; i++) {
        curpos = queue_incr(curpos);
        Bit b = timing_queue[curpos];
        code_dur += b.duration();
    }

    int32_t lower_bound = start.duration() * 7 / 64;
    int32_t bit_length = start.duration() / 8;
    int32_t upper_bound = start.duration() * 10 / 64;
    int32_t length_error = 0;

    if (!check_bound(code_dur / 24, lower_bound, upper_bound)) {
        return;
    }

    int curpos = startpos;
    for (int i = 0; i < 24; i++) {
        code <<= 1;
        badbits <<= 1;

        curpos = queue_incr(curpos);
        if (curpos == timing_queue_tail) {
            badbits |= 1;
            nbadbits += 1;
            continue;
        }

        Bit b = timing_queue[curpos];

        if (!check_bit_bound(b.duration(), lower_bound, upper_bound, length_error)) {

            // Check for the case where there was a dropout in the middle of a mark
            int nextpos = queue_incr(curpos);
            Bit next = timing_queue[nextpos];
            if (nextpos != timing_queue_tail &&
                check_bit_bound(b.duration() + next.duration(), lower_bound, upper_bound, length_error)) {
                b.mark = b.mark + b.space + next.space;
                b.space = next.space;
                curpos = nextpos;
            } else {
                badbits |= 1;
                nbadbits += 1;
            }
        }

        if ((b.mark - length_error) > b.space) {
            code |= 1;
        }

        length_error = b.duration() - bit_length;
    }

    if (nbadbits) {
        if (nbadbits > 12)
            return;

        if (nbadbits <= 4 && last_good_code != 0) {
            code = last_good_code | 0x80000000;
            nbadbits = 0;
        }
    } else {
        last_good_code = code;
        last_good_code_time = micros();
    }

    if (debug_begin(DEBUG_DECODED_TIMINGS)) {
        write32(code);
        write32(badbits);
        write16(queue_incr(curpos, -startpos));

        for (int cpos = queue_incr(startpos, -1); cpos != curpos;) {
            cpos = queue_incr(cpos);
            Bit b = timing_queue[cpos];

            write16(b.mark);
            write16(b.space);
        }
        udp.endPacket();
    }

    if (nbadbits == 0) {
        if (config_data.remote_port > 0) {
            IPAddress temp_ip(config_data.remote_dest);
            udp.beginPacket(temp_ip, config_data.remote_port);
            write8(0xFF);
            write32(code);
            udp.endPacket();
        }
    }
}

void check_rmtrecv_debug() {
    if (debug_flags) {
        if (time_since(last_debug_packet) > 200000 || (num_raw_timings >= 16)) {
            if (num_raw_timings && debug_begin(DEBUG_RAW_TIMINGS)) {
                noInterrupts();
                write16(spurious_interrupts);
                write8(num_raw_timings);
                for (int i = 0; i < num_raw_timings; i++) {
                    Bit& cur(raw_pulse_timings[i]);
                    write16(cur.mark);
                    write16(cur.space);
                }
                num_raw_timings = 0;
                interrupts();
                udp.endPacket();
            } else {
                last_debug_packet = micros();
                // Decrease packet count without sending a packet
                if (--debug_npackets == 0) {
                    debug_flags = 0;
                }
            }
        }
    }
}

void rmt433_check() {
    check_rmtrecv_active();
    check_timing_queue();
    check_rmtrecv_debug();
}

#else

void rmt433_init() { }
void rmt433_check() { }
bool rmt433_can_cpu_idle() { return true; }
void rmt433_set_debug(const IPAddress& raddr, uint16_t rport, int arg1, int arg2) { }


#endif
