/* -*- mode: c++ -*-
 *
 * homenode.cpp
 *
 * Copyright (C) 2022 Katie Rust (katie@ktpanda.org)
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "esp_homenode.h"
#include "tempsense.h"
#include "rmt433.h"

#include <EEPROM.h>

extern const char DATE_TEXT[33] PROGMEM;

static const char BANNER[] PROGMEM =
    "\r\nESP8266 relay control"
#if ENABLE_TEMPSENSE
    " / temp sense"
#endif
#if ENABLE_RMTRECV
    " / 433 Mhz remote receiver"
#endif
    "\r\n\r\n"
    "https://ktpanda.org/software/esp_homenode\r\n"
    "\r\n"
    ;

static const char HELP[] PROGMEM =
    "Commands:\r\n"
    "?, h, help - reprint help\r\n"
    "i, info    - show Wifi connection info\r\n"
    "v, vars    - show configuration variables\r\n"
    "s, save    - save configuration to flash\r\n"
    "b, boot    - reboot\r\n"
    "d, debug   - enable/disable debug\r\n"
#if ENABLE_TEMPSENSE
    "t, temp    - show temperature\r\n"
#endif
    "version    - show build date\r\n"
    "erase!     - erase configuration and reboot\r\n"
    "\r\n"
    "Variables:\r\n"
    "ssid=SSID      - Set Wifi SSID to connect to\r\n"
    "psk=PASSWD     - Set Wifi password\r\n"
    "debounce=CS    - Output debounce time, in hundredths of a second\r\n"
#if ENABLE_TEMPSENSE
    "poll=MS        - Set polling rate for temperature sensor in ms\r\n"
    "tempip=IP,\r\n"
    "tempport=PORT  - IP address / UDP port to send temperature data to\r\n"
#endif
#if ENABLE_RMTRECV
    "rmtip=IP,\r\n"
    "rmtport=PORT   - IP address / UDP port to send 433 Mhz remote data to\r\n"
#endif
    "gpioip=IP,\r\n"
    "gpioport=PORT   - IP address / UDP port to send GPIO button updates to\r\n"
    "relayp[1-4]=PIN - Pin to control relay 1-4 (or 'x' to disable) \r\n"
    "btnp[1-4]=PIN   - Input pin for button 1-4 (or 'x' to disable) \r\n"
    "ledp[1-4]=PIN   - LED pin for relay 1-4 (or 'x' to disable) \r\n"
    "drive[1-4]=y/n  - Whether to drive the pin high or low when active\r\n"
    "invert[1-4]=y/n - Whether to invert LED output\r\n"
    ;

static const char SHORT_HELP[] PROGMEM =
    "Type \"help\" for help\r\n"
    ;

struct CommandDef;
struct VariableDef;

typedef bool (*commandfunc)(CommandCtx& out, char* param, int param_len);
typedef bool (*getsetfunc)(CommandCtx& out, void* arg, unsigned int arg_size, char* val, int val_len);

union CommandName {
    char text[12];
    uint32_t i[3];

    bool set(const char* c, unsigned int n) {
        i[0] = i[1] = i[2] = 0;
        if (n > 12)
            return false;
        strncpy(text, c, n);
        return true;
    }

    bool operator==(const CommandName& o) {
        return i[0] == o.i[0] && i[1] == o.i[1] && i[2] == o.i[2];
    }
};

struct CommandDef {
    commandfunc run;
    CommandName name;
};

struct VariableDef {
    getsetfunc getset;
    void* var_ptr;
    unsigned int var_size;
    CommandName name;
};

extern const CommandDef command_table[];
extern const VariableDef variable_table[];

////////////////////////////////////////////////////////////////////////////////
// Serial
////////////////////////////////////////////////////////////////////////////////

char line_buf[100];
unsigned int line_buf_pos;
bool serial_ignore_nl = false;
bool command_refresh = true;
bool serial_debug = false;

////////////////////////////////////////////////////////////////////////////////
// Config
////////////////////////////////////////////////////////////////////////////////

ConfigData config_data;

////////////////////////////////////////////////////////////////////////////////
// Wifi
////////////////////////////////////////////////////////////////////////////////

WiFiUDP udp;

// Indicates whether or not wifi ssid and password is enabled
bool wifi_enabled = false;

// Indicates whether or not we were connected to wifi on the last loop
bool wifi_connected = false;

// IP address to send temperature
IPAddress tempsense_dest_addr;

// IP address to send temperature
IPAddress remote_dest_addr;

// IP address to send temperature
IPAddress gpio_dest_addr;

extern void reconfigure_wifi(Print& out);

////////////////////////////////////////////////////////////////////////////////
// GPIO / Relays
////////////////////////////////////////////////////////////////////////////////

class DebouncedOutput;
class DebouncedInput;

uint32_t last_loop_millis;

// Current state of the GPIO output pins

extern DebouncedOutput gpio_outputs[NUM_OUTPUTS];
extern DebouncedInput button_input[NUM_OUTPUTS];

////////////////////////////////////////////////////////////////////////////////
// Message printing
////////////////////////////////////////////////////////////////////////////////

void clear_prompt() {
    if (!command_refresh) {
        Serial.print("\r\033[K");
        command_refresh = true;
    }
}


////////////////////////////////////////////////////////////////////////////////
// GPIO functions
////////////////////////////////////////////////////////////////////////////////

class DebouncedGPIO {
protected:
    uint16_t lock_time;
    bool curval, rawval;
public:

    DebouncedGPIO() : lock_time(0), curval(false), rawval(false)  { }

    bool check(unsigned int elapsed_time) {
        if (lock_time > 0) {
            if (elapsed_time >= lock_time) {
                lock_time = 0;
                if (curval != rawval) {
                    curval = rawval;
                    return true;
                }
            } else {
                lock_time -= elapsed_time;
            }
        }
        return false;
    }

    bool getraw() { return rawval; }
    bool getval() { return curval; }
};

class DebouncedOutput : public DebouncedGPIO {
    uint32_t last_change_time;
public:
    DebouncedOutput() : last_change_time(0){ }

    void output(int pin, bool drive_high) {
        if (curval) {
            digitalWrite(pin, drive_high ? HIGH : LOW);
            pinMode(pin, OUTPUT);
        } else {
            pinMode(pin, INPUT);
        }
    }

    bool check(unsigned int elapsed_time) {
        if (DebouncedGPIO::check(elapsed_time)) {
            lock_time = config_data.debounce_centisecs * 10;
            return true;
        }
        return false;
    }

    void set_now(bool val) {
        if (val != curval) {
            curval = val;
            lock_time = config_data.debounce_centisecs * 10;
        }
    }

    void set(bool val, uint32_t age=0) {
        if (val != rawval) {
            rawval = val;
            if (age >= 0xFFFF) {
                last_change_time = 0;
            } else {
                last_change_time = millis() - (age << 4);
            }
            if (last_change_time == 0) last_change_time = 1;
            if (lock_time == 0) {
                set_now(val);
            }
        }
    }

    uint16_t time_since_change(uint32_t ctime) {
        if (last_change_time == 0) {
            return 0xFFFF;
        }
        uint32_t diff = ctime - last_change_time;
        if (diff > 0xFFFF0) {
            last_change_time = 0;
            return 0xFFFF;
        }
        return diff >> 4;
    }

};


class DebouncedInput : public DebouncedGPIO {
public:
    int input(int pin) {
        rawval = digitalRead(pin) == LOW;
        if (lock_time != 0)
            return 0;

        if (rawval == curval)
            return 0;

        curval = rawval;
        lock_time = 100;

        return rawval ? 1 : -1;
    }
};

void setpins() {
    for (int i = 0; i < NUM_OUTPUTS; i++) {
        RelayConfig& relay(config_data.relays[i]);
        if (valid_gpio(relay.relay_pin))
            gpio_outputs[i].output(gpio_pin(relay.relay_pin), relay.drive);
        if (valid_gpio(relay.led_pin)) {
            bool on = (relay.led_invert ^ gpio_outputs[i].getraw());

            analogWrite(gpio_pin(relay.led_pin), on ? config_data.led_onlevel : config_data.led_offlevel);
        }
    }
}


////////////////////////////////////////////////////////////////////////////////
// Setup
////////////////////////////////////////////////////////////////////////////////

// from http://stackoverflow.com/questions/10564491/function-to-calculate-a-crc16-checksum
static uint16_t crc16(const void* ptr, int length) {
    uint8_t x;
    uint16_t crc = 0xFFFF;

    const uint8_t* data_p = reinterpret_cast<const uint8_t*>(ptr);
    while (length--){
        x = crc >> 8 ^ *data_p++;
        x ^= x>>4;
        crc = (crc << 8) ^ ((uint16_t)(x << 12)) ^ ((uint16_t)(x <<5)) ^ ((uint16_t)x);
    }
    return crc;
}

void enforce_null_term() {
    config_data.wifi_ssid[WIFI_SSID_MAX - 1] = 0;
    config_data.wifi_passwd[WIFI_PASSWD_MAX - 1] = 0;
}

template<typename T>
static bool _read_check_config(T& config, uint16_t readcrc) {
    uint16_t calccrc;

    EEPROM.get(4, config);
    config.wifi_ssid[WIFI_SSID_MAX - 1] = 0;
    config.wifi_passwd[WIFI_PASSWD_MAX - 1] = 0;

    if (config.size < 2 || config.size > sizeof(config)) {
        Serial.printf("Config structure length invalid: %d\r\n", config.size);
        return false;
    }
    if (config.size < sizeof(config)) {
        memset(reinterpret_cast<uint8_t*>(&config) + config.size, 0,
               sizeof(config) - config.size);

    }
    calccrc = crc16(&config, config.size);
    if (calccrc != readcrc) {
        Serial.printf("Config CRC invalid: read %04X, calculated %04X\r\n", readcrc, calccrc);
        return false;
    }
    return true;
}

static bool _read_config() {
    uint16_t sig, readcrc;
    EEPROM.get(0, sig);
    EEPROM.get(2, readcrc);

    if (sig == CONFIG_SIG) {
        return _read_check_config<ConfigData>(config_data, readcrc);
    }

    if (sig == 0x1545) {
        ConfigData_1545 oldconfig;
        if (!_read_check_config<ConfigData_1545>(oldconfig, readcrc)) {
            return false;
        }
        memcpy(&config_data, &oldconfig, offsetof(ConfigData, debounce_centisecs));
        config_data.debounce_centisecs = oldconfig.debounce_centisecs;
        config_data.gpio_dest = oldconfig.gpio_dest;
        config_data.gpio_port = oldconfig.gpio_port;
        //save_config(Serial);
        return true;
    }

    Serial.printf("Config signature invalid: %04X\r\n", sig);
    return false;
}


bool read_config() {
    EEPROM.begin(512);
    bool res = _read_config();
    EEPROM.end();

    if (!res) {
        memset(&config_data, 0, sizeof(config_data));
    }

    if (config_data.led_onlevel == 0) {
        config_data.led_onlevel = 255;
    }

    tempsense_dest_addr = IPAddress(config_data.tempsense_dest);
    remote_dest_addr = IPAddress(config_data.remote_dest);
    gpio_dest_addr = IPAddress(config_data.gpio_dest);

    return res;
}

void save_config(Print& out) {
    uint16_t sig, calccrc;
    EEPROM.begin(512);
    sig = CONFIG_SIG;
    enforce_null_term();
    config_data.size = sizeof(config_data);
    calccrc = crc16(&config_data, sizeof(config_data));
    EEPROM.put(0, sig);
    EEPROM.put(2, calccrc);
    EEPROM.put(4, config_data);
    EEPROM.end();
    out.print("EEPROM data saved, CRC = ");
    out.println(calccrc, HEX);
}

void save_config() {
    save_config(Serial);
}

void _print_const(Print& out, const char* text, int size) {
    char tempbuf[size];
    memcpy_P(tempbuf, text, size);
    out.write(tempbuf, size);
}

void set_relay_pinmode(RelayConfig& relay) {
    if (valid_gpio(relay.button_pin))
        pinMode(gpio_pin(relay.button_pin), INPUT);
    if (valid_gpio(relay.led_pin))
        pinMode(gpio_pin(relay.led_pin), OUTPUT);
    if (valid_gpio(relay.relay_pin))
        pinMode(gpio_pin(relay.relay_pin), OUTPUT);
}

void setup() {
    Serial.begin(115200);
    line_buf_pos = 0;
    serial_ignore_nl = false;

    read_config();

    setpins();

    print_const(Serial, BANNER);
    print_const(Serial, DATE_TEXT);

    pinMode(LED2, OUTPUT);

    for (int i = 0; i < NUM_OUTPUTS; i++) {
        RelayConfig& relay(config_data.relays[i]);
        set_relay_pinmode(relay);
    }

    command_refresh = true;

    reconfigure_wifi(Serial);
    udp.begin(5747);

    tempsense_init();
    rmt433_init();

    print_const(Serial, SHORT_HELP);

    last_loop_millis = millis();
}

////////////////////////////////////////////////////////////////////////////////
// Main loop functions
////////////////////////////////////////////////////////////////////////////////

void send_gpio_packet(const IPAddress& dest, int port, uint8_t cmd, int which, uint32_t ctime) {
    udp.beginPacket(dest, port);
    udp.write(cmd);
    for (int i = 0; i < NUM_OUTPUTS; i++) {
        if (which & (1 << i)) {
            DebouncedOutput &gpio(gpio_outputs[i]);
            udp.write((gpio.getraw() ? 0x80 : 0) | i);
            write16(gpio.time_since_change(ctime));
        }
    }
    udp.endPacket();
}

void send_gpio_packet(uint8_t cmd, int which, uint32_t ctime) {
    if (serial_debug) {
        async_message(Serial, "pkt dest=", config_data.gpio_dest);
        async_message(Serial, "pkt port=", config_data.gpio_port);
    }
    if (config_data.gpio_dest && config_data.gpio_port) {
        IPAddress temp_ip(config_data.gpio_dest);
        send_gpio_packet(temp_ip, config_data.gpio_port, cmd, which, ctime);
    }
}

void check_recv_udp() {
    unsigned int size;
    if ((size = udp.parsePacket()) != 0) {
        IPAddress raddr = udp.remoteIP();
        uint16_t rport = udp.remotePort();
        if (size >= 1) {
            uint8_t cmd = udp.read();
            switch (cmd) {
                case 0x40: {
                    size -= 1;

                    char tmp_buf[100];
                    char* end;

                    if (size > sizeof(tmp_buf) - 1)
                        size = sizeof(tmp_buf) - 1;

                    udp.read(line_buf, size);
                    end = line_buf + size;
                    *end = 0;
                    while (end > line_buf && (end[-1] == '\r' || end[-1] == '\n')) *--end = 0;

                    udp.beginPacket(raddr, rport);
                    CommandCtx ctx(udp, true);
                    handle_command_line(ctx, line_buf, end - line_buf);
                    udp.endPacket();
                } break;
                case 0x41:
                case 0x42:
                case 0x43: {
                    uint32_t ctime = millis();
                    uint8_t need_response = (cmd == 0x41 ? (1<<NUM_OUTPUTS) - 1 : 0);
                    size -= 1;
                    while (size >= 3) {
                        size -= 3;
                        uint8_t gpiocmd = udp.read();
                        uint16_t their_age = read16();
                        uint8_t which = gpiocmd & 0x7F;
                        bool val = (gpiocmd & 0x80) != 0;
                        if (which < NUM_OUTPUTS) {
                            DebouncedOutput &gpio(gpio_outputs[which]);
                            if (gpio.getraw() != val) {
                                uint16_t our_age = gpio.time_since_change(ctime);
                                // We've changed ours more recently; send them an update
                                if (our_age < their_age) {
                                    need_response |= 1 << which;
                                } else {
                                    gpio.set(val, their_age);
                                }
                            }
                        }
                    }
                    if (need_response && cmd != 0x43) {
                        send_gpio_packet(raddr, rport, 0x43, need_response, ctime);
                    }
                } break;
                case 0xF0:
                    uint8_t arg1 = udp.read();
                    uint8_t arg2 = udp.read();
                    rmt433_set_debug(raddr, rport, arg1, arg2);
                    break;
            }
        }
    } else if (rmt433_can_cpu_idle()) {
        asm("waiti 0");
    }
}

void reconfigure_wifi(Print& out) {
    clear_prompt();
    if (config_data.wifi_ssid[0] && config_data.wifi_passwd[0]) {
        print_quoted(out, "Enabling Wifi with SSID", config_data.wifi_ssid);
        WiFi.begin(config_data.wifi_ssid, config_data.wifi_passwd);
        WiFi.reconnect();
        wifi_enabled = true;
    } else {
        WiFi.disconnect();
        out.println("No Wifi SSID/password has been configured");
        wifi_enabled = false;
    }
}

void check_wifi_connected() {

    if (WiFi.isConnected()) {
        if (!wifi_connected) {
            async_message(Serial, "Wifi connected");
            wifi_connected = true;
            send_gpio_packet(0x41, 0xFF, millis());
        }
        digitalWrite(LED2, HIGH);
    } else {
        if (wifi_connected) {
            async_message(Serial, "Wifi disconnected");
            wifi_connected = false;
        }
        if (wifi_enabled) {
            digitalWrite(LED2, ((millis() % 500) > 250) ? HIGH : LOW);
        } else {
            digitalWrite(LED2, ((millis() % 200) > 100) ? HIGH : LOW);
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
// Commands
////////////////////////////////////////////////////////////////////////////////

inline bool is_input_passwd() {
    return line_buf_pos > 4 && memcmp(line_buf, "psk=", 4) == 0;
}

bool command_help(CommandCtx& ctx, char* param UNUSED, int param_len UNUSED) {
    print_const(ctx.out, BANNER);
    print_const(ctx.out, HELP);
    return true;
}

bool command_save(CommandCtx& ctx, char* param UNUSED, int param_len UNUSED) {
    save_config(ctx.out);
    return true;
}

bool command_boot(CommandCtx& ctx, char* param UNUSED, int param_len UNUSED) {
    ctx.out.println("Rebooting");
    ESP.reset();
    return true;
}

bool command_version(CommandCtx& ctx, char* param UNUSED, int param_len UNUSED) {
    print_const(ctx.out, DATE_TEXT);
    return true;
}

bool command_erase(CommandCtx& ctx, char* param UNUSED, int param_len UNUSED) {
    ctx.out.println("Use \"erase!\" to erase config");
    return true;
}

bool command_debug(CommandCtx& ctx, char* param UNUSED, int param_len UNUSED) {
    serial_debug = !serial_debug;
    if (serial_debug) {
        ctx.out.println("Debug enabled");
    } else {
        ctx.out.println("Debug disabled");
    }

    return true;
}

bool command_erase_real(CommandCtx& ctx, char* param UNUSED, int param_len UNUSED) {
    if (ctx.is_net) {
        ctx.out.println("Can't erase config via Wifi");
        return true;
    }

    ctx.out.println("Erasing configuration...");
    memset(&config_data, 0, sizeof(config_data));
    save_config(ctx.out);
    ESP.reset();
    return true;
}

bool command_info(CommandCtx& ctx, char* param UNUSED, int param_len UNUSED) {
    print_quoted(ctx.out, "SSID        :", WiFi.SSID());
    print_unquoted(ctx.out, "BSSID       :", WiFi.BSSIDstr());
    print_unquoted(ctx.out, "MAC address :", WiFi.macAddress());
    print_unquoted(ctx.out, "Hostname    :", WiFi.hostname());
    print_unquoted(ctx.out, "IP address  :", WiFi.localIP());
    print_unquoted(ctx.out, "Subnet mask :", WiFi.subnetMask());
    print_unquoted(ctx.out, "Gateway     :", WiFi.gatewayIP());

    return true;
}

bool command_vars(CommandCtx& ctx, char* param UNUSED, int param_len UNUSED) {
    char tmpname[12];
    for (const VariableDef* var = variable_table; var->getset; var++) {
        memcpy(tmpname, var->name.text, 12);
        int i = 11;
        while (!tmpname[i]) tmpname[i--] = ' ';

        ctx.out.write(tmpname, 12);
        ctx.out.print(": ");
        var->getset(ctx, var->var_ptr, var->var_size, NULL, 0);
        ctx.out.println();
    }
    return true;
}

bool handle_variable(CommandCtx& ctx, char* name, int name_len,
                     char* val, int val_len, bool print_after_set) {
    CommandName tmpname;
    if (!tmpname.set(name, name_len))
        return false;

    for (const VariableDef* var = variable_table; var->getset; var++) {
        if (tmpname == var->name) {
            if (val) {
                bool res = var->getset(ctx, var->var_ptr, var->var_size, val, val_len);
                if (!res) {
                    ctx.out.print("Invalid value for ");
                    ctx.out.write(name, name_len);
                    ctx.out.print(": \"");
                    ctx.out.write(val, val_len);
                    ctx.out.println("\"");
                    return true;
                }
            }
            if (!val || print_after_set) {
                ctx.out.write(name, name_len);
                ctx.out.print(": ");
                var->getset(ctx, var->var_ptr, var->var_size, NULL, 0);
                ctx.out.println();
            }
            return true;
        }
    }
    return false;
}

bool handle_command(CommandCtx& ctx, char* name, int name_len,
                    char* param, int param_len) {

    CommandName tmpname;
    if (!tmpname.set(name, name_len))
        return false;

    for (const CommandDef* cmd = command_table; cmd->run; cmd++) {
        if (tmpname == cmd->name) {
            return cmd->run(ctx, param, param_len);
        }
    }
    return false;
}

bool handle_command_line(CommandCtx& ctx, char* cmdline, int cmdline_len) {
    for (char* p = cmdline, cmd_len = 0; cmd_len < cmdline_len; p++, cmd_len++) {
        switch(*p) {
            case ' ':
                return handle_command(ctx, cmdline, cmd_len,
                                      p + 1, cmdline_len - cmd_len - 1);
            case '=':
                return handle_variable(ctx, cmdline, cmd_len,
                                       p + 1, cmdline_len - cmd_len - 1, true);
        }
    }
    if (handle_command(ctx, cmdline, cmdline_len, NULL, 0))
        return true;
    if (handle_variable(ctx, cmdline, cmdline_len, NULL, 0, true))
        return true;

    print_quoted(ctx.out, "Invalid command:", cmdline);
    return false;
}

template<typename T>
bool getset_str(CommandCtx& ctx, T var, unsigned int var_size, char* str, int str_len) {
    if (str) {
        if (str_len > var_size - 1) {
            return false;
        }
        memcpy(var, str, str_len);
        var[str_len] = 0;
    } else {
        ctx.out.print(var);
    }
    return true;
}

template<typename T>
bool getset_wifistr(CommandCtx& ctx, T var, unsigned int var_size, char* str, int str_len) {
    if (str && ctx.is_net) return true;
    bool res = getset_str<T>(ctx, var, var_size, str, str_len);
    if (res && str) reconfigure_wifi(ctx.out);
    return res;
}

template<typename T>
bool getset_wifipsk(CommandCtx& ctx, T var, unsigned int var_size, char* str, int str_len) {
    if (!str) {
        ctx.out.print(var[0] ? "(set)" : "(unset)");
        return true;
    }
    return getset_wifistr<T>(ctx, var, var_size, str, str_len);
}

template<typename T>
bool getset_ipaddr(CommandCtx& ctx, T* var, unsigned int var_size UNUSED,
                   char* str, int str_len UNUSED) {
    IPAddress temp_ip;
    if (str) {
        if (!temp_ip.fromString(str)) {
            return false;
        }
        *var = temp_ip.v4();
    } else {
        temp_ip = *var;
        ctx.out.print(temp_ip);
    }
    return true;
}

template<typename T>
bool getset_dec(CommandCtx& ctx, T* var, unsigned int var_size UNUSED, char* str, int str_len) {
    if (str) {
        T curval = 0;
        for (const char* p = str; str_len; p++, str_len--) {
            T nextval = curval * 10;
            if ('0' <= *p && *p <= '9') {
                nextval += (*p - '0');
            } else {
                return false;
            }
            // overflow?
            if (nextval < curval) {
                return false;
            }
            curval = nextval;
        }
        *var = curval;
    } else {
        ctx.out.print(*var);
    }
    return true;
}

template<typename T>
bool getset_pin(CommandCtx& ctx, T* var, unsigned int var_size UNUSED, char* str, int str_len) {
    if (str) {
        if (str_len == 1 && str[0] == 'x') {
            *var = 0;
        }
        int val;
        if (!getset_dec<int>(ctx, &val, sizeof(int), str, str_len)) {
            return false;
        }
        if (val < 0 || val > NUM_GPIOS)
            return false;
        *var = val + 1;
    } else {
        if (*var == 0) {
            ctx.out.print('x');
        } else {
            ctx.out.print(*var - 1);
        }
    }
    return true;
}

template<typename T>
bool getset_bool(CommandCtx& ctx, T* var, unsigned int var_size UNUSED, char* str, int str_len) {
    if (str) {
        if (str_len == 1) {
            switch (str[0]) {
                case '1':
                case 't':
                case 'y':
                case 'T':
                case 'Y':
                    *var = true;
                    return true;
                case '0':
                case 'f':
                case 'n':
                case 'F':
                case 'N':
                    *var = false;
                    return true;
                default:
                    return false;
            }
        }
        return false;
    } else {
        ctx.out.print(*var ? 'y' : 'n');
    }
}

template<typename T>
bool getset_drive(CommandCtx& ctx, T* var, unsigned int var_size UNUSED, char* str, int str_len) {
    bool val = var->drive;
    if (!getset_bool(ctx, &val, sizeof(bool), str, str_len))
        return false;
    if (str) var->drive = val;
    return true;
}

template<typename T>
bool getset_invert(CommandCtx& ctx, T* var, unsigned int var_size UNUSED, char* str, int str_len) {
    bool val = var->led_invert;
    if (!getset_bool(ctx, &val, sizeof(bool), str, str_len))
        return false;
    if (str) var->led_invert = val;
    return true;
}

template<typename T>
bool getset_hex(CommandCtx& ctx, T* var, unsigned int var_size UNUSED, char* str, int str_len) {
    if (str) {
        T curval = 0;
        for (const char* p = str; str_len; p++, str_len--) {
            T nextval = curval * 16;
            if ('0' <= *p && *p <= '9') {
                nextval += (*p - '0');
            } else if ('a' <= *p && *p <= 'f') {
                nextval += (10 + *p - 'a');
            } else if ('F' <= *p && *p <= 'F') {
                nextval += (10 + *p - 'A');
            } else {
                return false;
            }
            // overflow?
            if (nextval < curval) {
                return false;
            }
            curval = nextval;
        }
        *var = curval;
    } else {
        ctx.out.print(*var, HEX);
    }
    return true;
}

void clear_input_buffer() {
    memset(line_buf, 0, sizeof(line_buf));
    line_buf_pos = 0;
    command_refresh = true;
}

void handle_input_char(char ch) {
    CommandCtx ctx(Serial, false);
    switch (ch) {
        case '\b':
        case '\x7f':
            if (line_buf_pos > 0) {
                line_buf[--line_buf_pos] = 0;
                Serial.write("\b \b");
            }
            break;

        case '\x0C': // Ctrl-L
            command_refresh = true;
            break;

        case '\x03': // Ctrl-C
            Serial.println("^C");
            clear_input_buffer();
            break;

        case '\r': // Enter
            Serial.println();
            if (line_buf_pos && !handle_command_line(ctx, line_buf, line_buf_pos)) {
                print_const(Serial, SHORT_HELP);
            }
            clear_input_buffer();
            break;

        case ' ':
            // Don't allow spaces at the beginning of a line
            if (line_buf_pos == 0)
                break;

        default:
            if (ch >= 32 && line_buf_pos < (sizeof(line_buf) - 1)) {
                line_buf[line_buf_pos++] = ch;
                line_buf[line_buf_pos] = 0;
                Serial.print(is_input_passwd() ? '*' : ch);
            }
            break;
    }
}

static void refresh_prompt() {
    Serial.print("\r\033[K> ");

    if (is_input_passwd()) {
        Serial.write(line_buf, 4);
        for (unsigned int i = 4; i < line_buf_pos; i++) {
            Serial.print('*');
        }
    } else {
        Serial.write(line_buf, line_buf_pos);
    }
}

#define COMMAND(name, run) { (commandfunc)command_##run, { name } }
#define COMMANDX(name) COMMAND(#name, name)

#define VARDEF_RAW(name, getset, argptr, argsize) \
    { (getsetfunc)getset, (void*)argptr, argsize, { name } }

#define VARDEF(name, xtype, var) \
    VARDEF_RAW(#name, getset_##xtype<std::decay<decltype(var)>::type>, &var, sizeof(var))

//ICACHE_RODATA_ATTR
const ICACHE_RODATA_ATTR CommandDef command_table[] = {
    COMMAND("i", info),
    COMMAND("s", save),
    COMMAND("v", vars),
    COMMAND("b", boot),
    COMMAND("h", help),
    COMMAND("d", debug),
    COMMAND("?", help),
    COMMANDX(info),
    COMMANDX(vars),
    COMMANDX(save),
    COMMANDX(boot),
    COMMANDX(help),
    COMMANDX(version),
    COMMANDX(erase),
    COMMANDX(debug),
#if ENABLE_TEMPSENSE
    COMMANDX(temp),
    COMMAND("t", temp),
#endif
    COMMAND("erase!", erase_real),
    { NULL, { "" } },
};

const ICACHE_RODATA_ATTR VariableDef variable_table[] = {
    VARDEF(ssid, wifistr, config_data.wifi_ssid),
    VARDEF(psk, wifipsk, config_data.wifi_passwd),
    VARDEF(debounce, dec, config_data.debounce_centisecs),
    VARDEF(ledon, dec, config_data.led_onlevel),
    VARDEF(ledoff, dec, config_data.led_offlevel),
#if ENABLE_TEMPSENSE
    VARDEF(poll, dec, config_data.tempsense_interval),
    VARDEF(tempip, ipaddr, config_data.tempsense_dest),
    VARDEF(tempport, dec, config_data.tempsense_port),
#endif
#if ENABLE_RMTRECV
    VARDEF(rmtip, ipaddr, config_data.remote_dest),
    VARDEF(rmtport, dec, config_data.remote_port),
#endif
    VARDEF(gpioip, ipaddr, config_data.gpio_dest),
    VARDEF(gpioport, dec, config_data.gpio_port),

    VARDEF(relayp1, pin, config_data.relays[0].relay_pin),
    VARDEF(btnp1, pin, config_data.relays[0].button_pin),
    VARDEF(ledp1, pin, config_data.relays[0].led_pin),
    VARDEF(drive1, drive, (config_data.relays[0])),
    VARDEF(invert1, invert, config_data.relays[0]),

    VARDEF(relayp2, pin, config_data.relays[1].relay_pin),
    VARDEF(btnp2, pin, config_data.relays[1].button_pin),
    VARDEF(ledp2, pin, config_data.relays[1].led_pin),
    VARDEF(drive2, drive, config_data.relays[1]),
    VARDEF(invert2, invert, config_data.relays[1]),

    VARDEF(relayp3, pin, config_data.relays[2].relay_pin),
    VARDEF(btnp3, pin, config_data.relays[2].button_pin),
    VARDEF(ledp3, pin, config_data.relays[2].led_pin),
    VARDEF(drive3, drive, config_data.relays[2]),
    VARDEF(invert3, invert, config_data.relays[2]),

    VARDEF(relayp4, pin, config_data.relays[3].relay_pin),
    VARDEF(btnp4, pin, config_data.relays[3].button_pin),
    VARDEF(ledp4, pin, config_data.relays[3].led_pin),
    VARDEF(drive4, drive, config_data.relays[3]),
    VARDEF(invert4, invert, config_data.relays[3]),

    { NULL, NULL, 0, { "" } },
};

DebouncedOutput gpio_outputs[NUM_OUTPUTS];
DebouncedInput button_input[NUM_OUTPUTS];

static void check_serial() {
    if (command_refresh) {
        refresh_prompt();
        command_refresh = false;
    }
    while (Serial.available() > 0) {
        char ch = Serial.read();
        if (ch == '\n') {
            if (serial_ignore_nl) {
                serial_ignore_nl = false;
                continue;
            }
            ch = '\r';
        } else if (ch == '\r') {
            serial_ignore_nl = true;
        } else {
            serial_ignore_nl = false;
        }

        handle_input_char(ch);
    }
}

static void check_gpios() {
    uint32_t ctime = millis();
    uint32_t elapsed_time = ctime - last_loop_millis;
    last_loop_millis = ctime;

    for (int i = 0; i < NUM_OUTPUTS; i++) {
        gpio_outputs[i].check(elapsed_time);
    }

    for (int i = 0; i < NUM_OUTPUTS; i++) {
        RelayConfig& relay(config_data.relays[i]);
        if (!valid_gpio(relay.button_pin))
            continue;

        button_input[i].check(elapsed_time);

        if (button_input[i].input(gpio_pin(relay.button_pin)) == 1) {
            DebouncedOutput &gpio(gpio_outputs[i]);
            gpio.set(!gpio.getraw());
            send_gpio_packet(0x41, (1 << i), millis());
        }
    }
    setpins();
}

void loop() {
    check_gpios();

    check_serial();
    check_recv_udp();

    check_wifi_connected();
    rmt433_check();
    tempsense_check();
}
