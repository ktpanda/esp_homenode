#ifndef ESP_TEMPSENSE_H
#define ESP_TEMPSENSE_H

extern void tempsense_init();
extern void tempsense_check();
extern bool command_temp(CommandCtx& ctx, char* param, int param_len);

#endif
