BUILD_DATE := $(shell date +'%Y-%m-%d %H:%M:%S')

build/esp_homenode.ino.bin:	esp_homenode/esp_homenode.h esp_homenode/rmt433.h \
	esp_homenode/tempsense.h esp_homenode/homenode.cpp esp_homenode/rmt433.cpp \
	esp_homenode/tempsense.cpp
	mkdir -p build/cache
	echo '#include <Arduino.h>\nextern const char DATE_TEXT[33] PROGMEM = "BUILD_DATE=$(BUILD_DATE)\\r\\n";' > esp_homenode/build_date.cpp
	arduino-cli compile --build-path build --build-cache-path build/cache \
		--warnings all --verbose -b esp8266:esp8266:nodemcuv2 esp_homenode
	echo "$(BUILD_DATE)" > build/build_date.txt
